package ru.tsc.korosteleva.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.korosteleva.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
