package ru.tsc.korosteleva.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ICommand {

    @NotNull
    String getName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

}
