package ru.tsc.korosteleva.tm.api.repository;

import org.jetbrains.annotations.NotNull;

public interface IProjectTaskRepository {

    void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void removeProjectById(@NotNull String userId, @NotNull String projectId);

}
