package ru.tsc.korosteleva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.api.service.IProjectService;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.*;
import ru.tsc.korosteleva.tm.exception.user.UserIdEmptyException;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.Date;
import java.util.Optional;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description,
                          @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        Optional.ofNullable(dateBegin).orElseThrow(DateIncorrectException::new);
        Optional.ofNullable(dateEnd).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description, dateBegin, dateEnd);
    }

    @NotNull
    @Override
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return Optional.ofNullable(repository.findOneByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project updateById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final String name,
                              @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return Optional.ofNullable(repository.updateById(userId, id, name, description))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project updateByIndex(@Nullable final String userId,
                                 @Nullable final Integer index,
                                 @Nullable final String name,
                                 @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.of(index)
                .filter(i -> i > 0 && i < repository.getSize(userId))
                .orElseThrow(IndexIncorrectException::new);
        return Optional.ofNullable(repository.updateByIndex(userId, index, name, description))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project removeByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return Optional.ofNullable(repository.removeByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(@Nullable final String userId,
                                           @Nullable final String id,
                                           @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return Optional.ofNullable(repository.changeProjectStatusById(userId, id, status))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(@Nullable final String userId,
                                              @Nullable final Integer index,
                                              @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.of(index)
                .filter(i -> i > 0 && i < repository.getSize(userId))
                .orElseThrow(IndexIncorrectException::new);
        return Optional.ofNullable(repository.changeProjectStatusByIndex(userId, index, status))
                .orElseThrow(ProjectNotFoundException::new);
    }

}