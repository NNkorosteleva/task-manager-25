package ru.tsc.korosteleva.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    private String userId;

}
