package ru.tsc.korosteleva.tm.exception.user;

public final class LoginEmptyException extends AbstractUserException {

    public LoginEmptyException() {
        super("Error! Login is empty.");
    }

}
