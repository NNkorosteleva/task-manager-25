package ru.tsc.korosteleva.tm.exception.system;

import ru.tsc.korosteleva.tm.exception.AbstractException;

public class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command not supported.");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command ``" + command + "`` is not supported.");
    }
}
