package ru.tsc.korosteleva.tm.exception.user;

import ru.tsc.korosteleva.tm.exception.AbstractException;

public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException() {
    }

    public AbstractUserException(String message) {
        super(message);
    }

}
