package ru.tsc.korosteleva.tm.exception.user;

public final class LoginExistException extends AbstractUserException {

    public LoginExistException(String value) {
        super("Error! Login ``" + value + "`` is exist.");
    }

}
