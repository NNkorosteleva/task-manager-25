package ru.tsc.korosteleva.tm.command.project;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-index";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    public static final String DESCRIPTION = "Start project by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}
