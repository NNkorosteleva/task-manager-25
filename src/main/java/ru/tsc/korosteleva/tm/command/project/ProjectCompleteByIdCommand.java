package ru.tsc.korosteleva.tm.command.project;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-complete-by-id";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    public static final String DESCRIPTION = "Complete project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
    }

}
