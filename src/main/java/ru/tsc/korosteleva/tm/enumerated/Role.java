package ru.tsc.korosteleva.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Role {

    ADMIN("Adminastrator"),
    USUAL("Usual user");

    @Getter
    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Role role : values()) {
            if (role.name().equals(value)) return role;
        }
        return null;
    }

}
